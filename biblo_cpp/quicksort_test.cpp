/**
 * quicksort_test.cpp
 * 
 * <P> Will test the quicksort algo
 * 
 * @author gchapman
 */

#include <gtest/gtest.h>
#include "quicksort.hpp"
#include <vector>

TEST(quicksort_test, nosort) 
{
    quicksort<int>().execute(NULL, 12);
}

TEST(quicksort_test, sortSmall) 
{
    std::vector<int> unsorted;
    unsorted.push_back(4);
    quicksort<int>().execute(&unsorted[0], unsorted.size());
    
    ASSERT_TRUE(unsorted.size() == 1);
    EXPECT_EQ(4, unsorted[0]);
}

TEST(quicksort_test, sortSimple) 
{
    std::vector<int> unsorted;
    unsorted.push_back(4);
    unsorted.push_back(5);
    unsorted.push_back(2);
    unsorted.push_back(10);
    unsorted.push_back(42);
    unsorted.push_back(41);
    unsorted.push_back(24);
    unsorted.push_back(43);
    unsorted.push_back(14);
    unsorted.push_back(454);
    unsorted.push_back(49);
    unsorted.push_back(8);
    
    quicksort<int>().execute(&unsorted[0], unsorted.size());
    
    ASSERT_TRUE(unsorted.size() == 12);
    EXPECT_EQ(2, unsorted[0]);
    EXPECT_EQ(4, unsorted[1]);
    EXPECT_EQ(5, unsorted[2]);
    EXPECT_EQ(8, unsorted[3]);
    EXPECT_EQ(10, unsorted[4]);
    EXPECT_EQ(14, unsorted[5]);
    EXPECT_EQ(24, unsorted[6]);
    EXPECT_EQ(41, unsorted[7]);
    EXPECT_EQ(42, unsorted[8]);
    EXPECT_EQ(43, unsorted[9]);
    EXPECT_EQ(49, unsorted[10]);
    EXPECT_EQ(454, unsorted[11]);
}

TEST(quicksort_test, sortSorted) 
{
    std::vector<int> unsorted;
    unsorted.push_back(1);
    unsorted.push_back(2);
    unsorted.push_back(3);
    unsorted.push_back(4);
    unsorted.push_back(5);
    unsorted.push_back(6);
    unsorted.push_back(7);
    unsorted.push_back(8);
    unsorted.push_back(9);
    unsorted.push_back(10);
    unsorted.push_back(11);
    unsorted.push_back(12);
    
    quicksort<int>().execute(&unsorted[0], unsorted.size());
    
    ASSERT_TRUE(unsorted.size() == 12);
    EXPECT_EQ(1, unsorted[0]);
    EXPECT_EQ(2, unsorted[1]);
    EXPECT_EQ(3, unsorted[2]);
    EXPECT_EQ(4, unsorted[3]);
    EXPECT_EQ(5, unsorted[4]);
    EXPECT_EQ(6, unsorted[5]);
    EXPECT_EQ(7, unsorted[6]);
    EXPECT_EQ(8, unsorted[7]);
    EXPECT_EQ(9, unsorted[8]);
    EXPECT_EQ(10, unsorted[9]);
    EXPECT_EQ(11, unsorted[10]);
    EXPECT_EQ(12, unsorted[11]);
}
