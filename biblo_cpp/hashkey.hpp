/**
 * hashkey.hpp
 * 
 * <P> Implements a simple primitive value hash keys.
 * 
 * @author gchapman
 */

#ifndef HASHKEY_HPP_145800
#define	HASHKEY_HPP_145800

#include <string>

template<class type>
struct hashkey
{
    /**
     * Will return a hash key for the type passed in
     * @param val Value to generate key for
     * @return Hash key
     */
    unsigned int operator()(const type& val)
    {
        return (unsigned int) val*2654435761;
    }
};

/**
 * Template specialization for string hash key.
 * Does a simple key = key x 101 + str[i] loop.
 * @param str String to hash
 * @return Hash key for string.
 */
template<>
unsigned int hashkey<std::string>::operator()(const std::string& str)
{
    unsigned int key = 2166136261;
    for(unsigned int i = 0; i < str.size(); ++i)
    {
        key = key * 101 + str[i];
    }
    return key;
}

#endif	/* HASHKEY_HPP_145800 */

