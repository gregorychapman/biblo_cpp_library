/**
 * mergesort.hpp
 * 
 * <P> Implements a standard merge sort implementation. Runtime O(n log n) and
 * memory usage is O(n). This could be more efficient by not creating extra 
 * storage for left and right. For now it provides a simple mergesort.
 * 
 * @author gchapman
 */

#ifndef MERGESORT_HPP_145800
#define	MERGESORT_HPP_145800

template<class type>
class mergesort
{
public:
    /**
     * Will execute a simple mergesort. Assumes the source is contiguous storage.
     * @param source Source of data to sort
     * @param count Size of content in source.
     */
    void execute(type* source, unsigned int count)
    {
        sort(source, count);
    }

private:
   
    void copy(type* source, type* destination, unsigned int first, unsigned int last)
    {
        unsigned int i = 0;
        while(first != last)
        {
            destination[i++] = source[first++];
        }
    }
    
    void merge(type* source, type* left, unsigned int l_length,
               type* right, unsigned int r_length)
    {
        unsigned int i = 0;
        unsigned int left_i = 0;
        unsigned int right_i = 0;
        while(left_i < l_length && right_i < r_length)
        {
            if(left[left_i] < right[right_i])
            {
                source[i++] = left[left_i++];
            }
            else
            {
                source[i++] = right[right_i++];
            }
        }
        
        while(left_i < l_length)
        {
            source[i++] = left[left_i++];
        }
        while(right_i < r_length)
        {
            source[i++] = right[right_i++];
        }
    }
    
    void sort(type* source, unsigned int count)
    {
        if(count < 2 || source == NULL)
        {
            return;
        }
        
        unsigned int mid = count/2;
        
        type* left = new type[mid];
        type* right = new type[count-mid];
        
        copy(source, left, 0, mid);
        copy(source, right, mid, count);
        
        unsigned int l_length = mid;
        unsigned int r_length = count-mid;
        sort(left, l_length);
        sort(right, r_length);
        merge(source, left, l_length, right, r_length);
        delete [] left;
        delete [] right;
    }
};

#endif	/* MERGESORT_HPP_145800 */

