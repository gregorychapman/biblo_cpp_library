/**
 * quicksort.hpp
 * 
 * <P> Implements a random pivot quick sort implementation. Runtime O(n log n)
 * with worst case O(n^2) and memory usage is O(1). This could be quicker than
 * mergesort because of the smaller constants in the runtime.
 * 
 * @author gchapman
 */

#ifndef QUICKSORT_HPP_145800
#define	QUICKSORT_HPP_145800

#include <stdlib.h>
#include <time.h>

template<class type>
class quicksort
{
public:
    /**
     * Will execute quicksort. Assumes the source is contiguous storage.
     * @param source Source of data to sort
     * @param count Size of content in source.
     */
    void execute(type* source, unsigned int count)
    {
        if(source == NULL) { return; }
        srand(time(NULL)); //seed the rand function
        sort(source, 0, count);
    }

private:
    
    void swap(type* t1, type* t2)
    {
        type tmp = *t1;
        *t1 = *t2;
        *t2 = tmp;
    }
    
    /**
     * Sorts recursively about some randomly chosen pivot value.
     * @param source Origin of the sorted types
     * @param index Index starting the content, included
     * @param count Size of items in the source to execute on
     */
    void sort(type* source, unsigned int index, unsigned int count)
    {
        if(count < 2) { return; }
        
        unsigned int pivotIndex = index + (rand() % count);
        type pivot = source[pivotIndex];
        swap(&source[index], &source[pivotIndex]); //move pivot to front for now
        
        unsigned int endIndex = index+count;
        unsigned int left, right;
        left = right = index+1;
        while(right < endIndex)
        {
            if(pivot > source[right])
            {
                swap(&source[left], &source[right]); 
                ++left;
            }
            ++right;
        }
        unsigned int middle = left-1;
        swap(&source[index], &source[middle]); //move back pivot to middle
        sort(source, index, middle-index); //recurse on left
        sort(source, left, endIndex-left); //recurse on right
    }
};

#endif	/* QUICKSORT_HPP_145800 */

