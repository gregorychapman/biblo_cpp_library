/**
 * hashtable.hpp
 * 
 * <P> Implements a simple hashtable implementation. Will use buckets which are
 * implemented with lists for collisions.
 * 
 * @author gchapman
 */

#ifndef HASHTABLE_HPP_145800
#define	HASHTABLE_HPP_145800

#include "list.hpp"
#include "pair.hpp"
#include "hashkey.hpp"

template<class key, class value>
class hashtable_iterator;

/**
 * Implementation of a bucket based hashtable.
 */
template<class key, class value>
class hashtable
{
public:
    
    typedef list<pair<key, value> > bucket;
    typedef hashtable_iterator<key,value> iterator;
    
    /**
     * hashtable constructor
     */
    hashtable();
    
    /**
     * Will construct and copy
     * @param copy Will copy this table.
     */
    hashtable(const hashtable& copy);
    
    /**
     * Will clear this table and copy the table passed in.
     * @param copy Table to copy
     * @return *this
     */
    hashtable& operator=(const hashtable& copy);
    
    /**
     * Destructor for hashtable
     */
    virtual ~hashtable();
    
    /**
     * Will erase the key from the table
     * @param k Key to erase
     */
    void erase(const key& k);
    
    /**
     * Will erase all data from table, keeps same table size.
     * @param k Key to erase
     */
    void eraseAll();
    
    /**
     * Key access to value. Use this to lookup a value given a key.
     * @param k Key to lookup or create in hashtable
     * @return Reference to the value found at the key.
     */
    value& operator[](const key& k);
    
    /**
     * Determine if a key exists in the table.
     * @param k Key to lookup.
     * @return True if the key is in the table. False otherwise.
     */
    bool contains(const key& k);
    
    /**
     * Will return the iterator for the hashtable. Provides a way to iterate
     * collection.
     * @return Beginning of the iteration.
     */
    iterator begin() const;
    
    /**
     * @return The number of key entries in the table.
     */
    inline unsigned int getSize() { return mEntries; }
    
protected:
    
    typename hashtable<key, value>::bucket::iterator 
        find(const key& k, bucket& bucket);
    unsigned int insertAll(bucket* destination,
                            unsigned int dsize,
                            bucket* source,
                            unsigned int size);
    value& insert(bucket* destination, unsigned int size,
                   const key& k, value& v, unsigned int& entries);
    bool rehash();
    void resize(unsigned int count);
    static double defaultRehashRatio() { return 0.75; }

    bucket* mContent;
    unsigned int mSize;
    unsigned int mEntries;
    bool mRehashing;

    static const int DEFAULT_SIZE = 10;
    static const int DEFAULT_RESIZE_FACTOR = 2;
};

template<class key, class value>
hashtable<key, value>::hashtable() :
    mContent(new bucket[DEFAULT_SIZE]),
    mSize(DEFAULT_SIZE),
    mEntries(0),
    mRehashing(false)
{
}

template<class key, class value>
hashtable<key, value>::~hashtable()
{
    delete [] mContent;
}

template<class key, class value>
hashtable<key, value>::hashtable(const hashtable<key, value>& copy) :
    mContent(new bucket[copy.mSize]),
    mSize(copy.mSize),
    mEntries(0),
    mRehashing(false)
{
    *this = copy;
}
    
template<class key, class value>
hashtable<key, value>& hashtable<key, value>::operator=(const hashtable<key, value>& copy)
{
    eraseAll();
    iterator iter = copy.begin();
    while(!iter.isEnd())
    {
        (*this)[iter.getValue().first] = iter.getValue().second;
        iter = iter.getNext();
    }
    return *this;
}

template<class key, class value>
void hashtable<key, value>::resize(unsigned int count)
{
    delete [] mContent;
    mContent = new bucket[count];
    mSize = count;
}

template<class key, class value>
void hashtable<key, value>::erase(const key& k)
{
    unsigned int index = hashkey<key>()(k) % mSize;
    bucket& bucket = mContent[index];
    typename bucket::iterator found(find(k, bucket));
    if(!found.isEnd())
    {
        --mEntries;
        bucket.erase(found);
    }
}

template<class key, class value>
void hashtable<key, value>::eraseAll()
{
    mEntries = 0;
    for(unsigned int i = 0; i < mSize; ++i)
    {
        mContent[i].eraseAll();
    }
}

template<class key, class value>
bool hashtable<key, value>::contains(const key& k)
{
    unsigned int index = hashkey<key>()(k) % mSize;
    bucket& bucket = mContent[index];
    
    typename hashtable<key, value>::bucket::iterator found = find(k, bucket);
    return !found.isEnd();
}

template<class key, class value>
typename hashtable<key, value>::bucket::iterator hashtable<key, value>::find
    (const key& k, bucket& bucket)
{
    typename hashtable<key, value>::bucket::iterator iter = bucket.begin();
    while(!iter.isEnd())
    {
        if(iter.getValue()->first == k)
        {
            return iter;
        }
        iter = iter.getNext();
    }
    return typename hashtable<key, value>::bucket::iterator();
}

template<class key, class value>
value& hashtable<key, value>::operator[](const key& k)
{
    value newValue;
    return insert(mContent, mSize, k, newValue, mEntries);
}

template<class key, class value>
value& hashtable<key, value>::insert(bucket* destination, unsigned int size, 
                   const key& k, value& v, unsigned int& entries)
{
    unsigned int index = hashkey<key>()(k) % size;
    bucket& bucket = destination[index];
    typename hashtable<key, value>::bucket::iterator found = find(k, bucket);
    if (found.isEnd())
    {
        ++entries;
        if (rehash()) { return (*this)[k]; }
        
        bucket.pushBack(pair<key, value>(k, v));
        return bucket.back().getValue()->second;
    }
    return found.getValue()->second;
}

template<class key, class value>
unsigned int hashtable<key, value>::insertAll(bucket* destination,
                                               unsigned int dsize,
                                               bucket* source,
                                               unsigned int size)
{
    unsigned int entries = 0;
    for(unsigned int i = 0; i < size; ++i) 
    {
        bucket& bucket = source[i];
        typename hashtable<key, value>::bucket::iterator iter = bucket.begin();
        while(!iter.isEnd())
        {
            insert(destination, dsize, iter.getValue()->first, 
                   iter.getValue()->second, entries);
            iter = iter.getNext();
        }
    }
    return entries;
}

template<class key, class value>
bool hashtable<key, value>::rehash()
{
    if(mRehashing) { return false; }
    
    double loadfactor = mEntries / (double) mSize;
    if (loadfactor >= defaultRehashRatio())
    {
        unsigned int newSize = mSize*DEFAULT_RESIZE_FACTOR;
        bucket* newContent = new bucket[newSize];
        mRehashing = true;
        mEntries = insertAll(newContent, newSize, mContent, mSize);
        mRehashing = false;
        bucket* old = mContent;
        mContent = newContent;
        mSize = newSize;
        delete [] old;
        return true;
    }
    return false;
}

template<class key, class value>
hashtable_iterator<key, value> hashtable<key, value>::begin() const
{
    return hashtable<key,value>::iterator(mContent, mSize);
}

/**
 * Implementation of the hashtable iterator to separate the iteration
 * logic from the hashtable. Will be invalid on hashtable erases or inserts.
 */
template<class key, class value>
class hashtable_iterator
{
public:
    
    /**
     * Construct an empty iterator
     */
    hashtable_iterator();
    
    /**
     * Will initialize an iterator from the hashtable, providing data.
     * @param content Content block of the hashtable
     * @param contentSize Size of the content block
     */
    hashtable_iterator(typename hashtable<key, value>::bucket* content,
                         unsigned int contentSize);
    /**
     * Will get the next iterator.
     * @return The next iterator in the table.
     */
    hashtable_iterator getNext();
    
    /**
     * Will get the value of the iterator.
     * @return Returns a const pair.
     */
    const pair<key, value>& getValue();
    
    /**
     * @return Returns true if this is truly the end of the iteration.
     */
    bool isEnd();
    
private:
    
    void findNextIterator();
    
    typename hashtable<key, value>::bucket* mContent;
    unsigned int mSize;
    unsigned int mIndex;
    list_iterator<pair<key, value> > mBucketIterator;
};

template<class key, class value>
hashtable_iterator<key, value>::hashtable_iterator() :
    mContent(NULL),
    mSize(0),
    mIndex(0)
{
}

template<class key, class value>
hashtable_iterator<key, value>::hashtable_iterator
    (typename hashtable<key, value>::bucket* content, unsigned int contentSize) :
    mContent(content),
    mSize(contentSize),
    mIndex(0)
    
{
    findNextIterator();
}

template<class key, class value>
hashtable_iterator<key, value> hashtable_iterator<key, value>::getNext()
{
    mBucketIterator = mBucketIterator.getNext();
    if(mBucketIterator.isEnd())
    {
        ++mIndex;
        findNextIterator();
    }
    return *this;
}

template<class key, class value>
const pair<key, value>& hashtable_iterator<key, value>::getValue()
{
    return *mBucketIterator.getValue();
}

template<class key, class value>
void hashtable_iterator<key, value>::findNextIterator()
{
    list_iterator<pair<key, value> > iter;
    for(unsigned int i = mIndex; i < mSize; ++i)
    {
        iter = mContent[i].begin();
        if(!iter.isEnd())
        {
            mBucketIterator = iter;
            mIndex = i;
            return;
        }
    }
    mBucketIterator = list_iterator<pair<key, value> >();
}

template<class key, class value>
bool hashtable_iterator<key, value>::isEnd()
{
    return mBucketIterator.isEnd();
}

#endif	/* HASHTABLE_HPP_145800 */

