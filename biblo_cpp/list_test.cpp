#include <gtest/gtest.h>
#include "list.hpp"

TEST(list_test, pushBack)
{
    list<std::string> test;
    
    test.pushBack("yes");
    test.pushBack("this works");
    
    list_iterator<std::string> iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());
    
    iter = iter.getNext();
    
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    
    test.eraseAll();
    
    iter = test.begin();
    EXPECT_TRUE(iter.isEnd());
}

TEST(list_test, copyConstructor)
{
    list<std::string> test;
    
    test.pushBack("yes");
    test.pushBack("this works");
    test.pushBack("this works1");
    test.pushBack("this works2");
    test.pushBack("this works3");
    
    list<std::string> test1(test);
    
    list_iterator<std::string> iter = test1.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works3", *iter.getValue());
    
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works3", *iter.getValue());
}

TEST(list_test, copyAndErase)
{
    list<std::string> test;
    
    test.pushBack("yes");
    test.pushBack("this works");
    test.pushBack("this works1");
    test.pushBack("this works2");
    test.pushBack("this works3");
    
    list<std::string> test1(test);
    
    //erase from test, check test1 is ok
    test.erase(test.back());
    
    
    list_iterator<std::string> iter = test1.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works3", *iter.getValue());
    
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    iter = iter.getNext();
    EXPECT_TRUE(iter.isEnd());//erased back
}

TEST(list_test, erase)
{
    list<std::string> test;
    
    test.erase(test.back());
    test.erase(test.begin());
    
    test.pushBack("yes");
    test.pushBack("this works");
    test.pushBack("this works1");
    test.pushBack("this works2");
    test.pushBack("this works3");
    
    //erase back
    test.erase(test.back());
    
    list_iterator<std::string> iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    iter = iter.getNext();
    EXPECT_TRUE(iter.isEnd());//erased back
    
    //erase head
    iter = test.begin();
    iter = test.erase(iter);
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    iter = iter.getNext();
    EXPECT_TRUE(iter.isEnd());//erased back
    
    //erase tail
    iter = test.begin();
    iter = iter.getNext();
    iter = iter.getNext();
    iter = test.erase(iter);
    ASSERT_TRUE(iter.isEnd());
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_TRUE(iter.isEnd());
    
    test.pushBack("this works2");
    
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    iter = iter.getNext();
    EXPECT_TRUE(iter.isEnd());
    
    EXPECT_EQ("this works2", *test.back().getValue());
    
    //erase common
    iter = test.begin();
    iter = iter.getNext();
    iter = test.erase(iter);
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    iter = iter.getNext();
    EXPECT_TRUE(iter.isEnd());

    //erase 1 item list
    iter = test.erase(test.begin());
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works2", *iter.getValue());
    
    iter = test.erase(test.begin());
    ASSERT_TRUE(iter.isEnd());
    
    //erase back w 1 item list
    test.insert("yes");
    
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());
    iter = iter.getNext();
    EXPECT_TRUE(iter.isEnd());
    
    iter = test.erase(test.back());
    ASSERT_TRUE(iter.isEnd());
    
    iter = test.begin();
    EXPECT_TRUE(iter.isEnd());
    
    test.insert("yes");
    
    iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());    
    iter = iter.getNext();
    EXPECT_TRUE(iter.isEnd());
    
    iter = test.erase(test.begin());
    ASSERT_TRUE(iter.isEnd());
    
    iter = test.begin();
    EXPECT_TRUE(iter.isEnd());
}

TEST(list_test, insert)
{
    list<std::string> test;
    
    test.insert("yes");
    test.insert("this works");
    
    list_iterator<std::string> iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("this works", *iter.getValue());
    
    iter = iter.getNext();
    
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("yes", *iter.getValue());
    
    test.eraseAll();
    
    iter = test.begin();
    EXPECT_TRUE(iter.isEnd());
}

TEST(list_test, insertSorted)
{
    list<std::string> test;
    
    test.insertSorted("a1");
    test.insertSorted("c");
    test.insertSorted("b");
    test.insertSorted("d");
    test.insertSorted("a");
    
    list_iterator<std::string> iter = test.begin();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("a", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("a1", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("b", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("c", *iter.getValue());
    iter = iter.getNext();
    ASSERT_FALSE(iter.isEnd());
    EXPECT_EQ("d", *iter.getValue());
    iter = iter.getNext();

    test.eraseAll();
    
    iter = test.begin();
    EXPECT_TRUE(iter.isEnd());
}
