/**
 * hashtable_test.cpp
 * 
 * <P> Will test the hashtable 
 * 
 * @author gchapman
 */

#include <gtest/gtest.h>
#include <string>
#include "hashtable.hpp"

TEST(hashtable_test, copy)
{
    hashtable<std::string, int> table;
    table["yes"] = 100;
    table["no"] = 1;
    table["no1"] = 2;
    table["no2"] = 3;
    table["no3"] = 4;
    table["no4"] = 5;
    table["no5"] = 6;
    table["no6"] = 7;
    EXPECT_EQ(100, table["yes"]);
    EXPECT_EQ(1, table["no"]);
    EXPECT_EQ(2, table["no1"]);
    EXPECT_EQ(3, table["no2"]);
    EXPECT_EQ(4, table["no3"]);
    EXPECT_EQ(5, table["no4"]);
    EXPECT_EQ(6, table["no5"]);
    EXPECT_EQ(7, table["no6"]);
    
    hashtable<std::string, int> table1(table);
    EXPECT_EQ(100, table1["yes"]);
    EXPECT_EQ(1, table1["no"]);
    EXPECT_EQ(2, table1["no1"]);
    EXPECT_EQ(3, table1["no2"]);
    EXPECT_EQ(4, table1["no3"]);
    EXPECT_EQ(5, table1["no4"]);
    EXPECT_EQ(6, table1["no5"]);
    EXPECT_EQ(7, table1["no6"]);
    
    EXPECT_EQ(8, table.getSize());
    EXPECT_EQ(8, table1.getSize());
    
    table.eraseAll();
    
    EXPECT_EQ(0, table.getSize());
    EXPECT_EQ(8, table1.getSize());
    
    EXPECT_EQ(6, table1["no5"]);
    EXPECT_EQ(7, table1["no6"]);
    
    table["yes"] = 10;
    
    table = table1;
    EXPECT_EQ(100, table["yes"]);
}

TEST(hashtable_test, insertKV)
{
    hashtable<std::string, int> table;
    table["yes"] = 100;
    table["no"] = 1;
    table["no1"] = 2;
    table["no2"] = 3;
    table["no3"] = 4;
    table["no4"] = 5;
    table["no5"] = 6;
    table["no6"] = 7;
    EXPECT_EQ(100, table["yes"]);
    EXPECT_EQ(1, table["no"]);
    EXPECT_EQ(2, table["no1"]);
    EXPECT_EQ(3, table["no2"]);
    EXPECT_EQ(4, table["no3"]);
    EXPECT_EQ(5, table["no4"]);
    EXPECT_EQ(6, table["no5"]);
    EXPECT_EQ(7, table["no6"]);
}

TEST(hashtable_test, eraseAll)
{
    hashtable<std::string, int> table;
    table["yes"] = 100;
    table["no"] = 1;
    table["no1"] = 2;
    table["no2"] = 3;
    table["no3"] = 4;
    table["no4"] = 5;
    table["no5"] = 6;
    table["no6"] = 7;
    EXPECT_EQ(100, table["yes"]);
    EXPECT_EQ(1, table["no"]);
    EXPECT_EQ(2, table["no1"]);
    EXPECT_EQ(3, table["no2"]);
    EXPECT_EQ(4, table["no3"]);
    EXPECT_EQ(5, table["no4"]);
    EXPECT_EQ(6, table["no5"]);
    EXPECT_EQ(7, table["no6"]);
    
    EXPECT_TRUE(table.contains("yes"));
    table.eraseAll();
    EXPECT_FALSE(table.contains("yes"));
    EXPECT_FALSE(table.contains("no"));
    EXPECT_FALSE(table.contains("no1"));
    EXPECT_FALSE(table.contains("no2"));
    EXPECT_FALSE(table.contains("no3"));
    EXPECT_FALSE(table.contains("no4"));
    EXPECT_FALSE(table.contains("no5"));
    EXPECT_FALSE(table.contains("no6"));
    EXPECT_EQ(0, table.getSize());
    
    table["no6"] = 7;
    EXPECT_EQ(7, table["no6"]);
    EXPECT_TRUE(table.contains("no6"));
    EXPECT_EQ(1, table.getSize());
}

TEST(hashtable_test, erasekey)
{
    hashtable<std::string, int> table;
    table["yes"] = 100;
    table["no"] = 1;
    table["no1"] = 2;
    table["no2"] = 3;
    table["no3"] = 4;
    table["no4"] = 5;
    table["no5"] = 6;
    table["no6"] = 7;
    EXPECT_EQ(100, table["yes"]);
    EXPECT_EQ(1, table["no"]);
    EXPECT_EQ(2, table["no1"]);
    EXPECT_EQ(3, table["no2"]);
    EXPECT_EQ(4, table["no3"]);
    EXPECT_EQ(5, table["no4"]);
    EXPECT_EQ(6, table["no5"]);
    EXPECT_EQ(7, table["no6"]);
    
    EXPECT_TRUE(table.contains("yes"));
    table.erase("yes");
    EXPECT_FALSE(table.contains("yes"));
    
    //should not be in the table
    EXPECT_FALSE(table.contains("yes1"));
    table.erase("yes1");
    EXPECT_FALSE(table.contains("yes1"));
    
    EXPECT_EQ(7, table.getSize());
    
    table.erase("yes");
    EXPECT_EQ(7, table.getSize());
}

TEST(hashtable_test, iterator)
{
    hashtable<std::string, int> table;
    table["yes"] = 100;
    table["no"] = 1;
    table["no1"] = 2;
    table["no2"] = 3;
    table["no3"] = 4;
    table["no4"] = 5;
    table["no5"] = 6;
    table["no6"] = 7;
    table["no6"] = 64;
    
    unsigned int count = 0;
    hashtable<std::string, int>::iterator iter = table.begin();
    while(!iter.isEnd())
    {
        ++count;
        if(iter.getValue().first == "yes")
        {
            EXPECT_EQ(100, iter.getValue().second);
        }
        else if(iter.getValue().first == "no")
        {
            EXPECT_EQ(1, iter.getValue().second);
        }
        else if(iter.getValue().first == "no1")
        {
            EXPECT_EQ(2, iter.getValue().second);
        }
        else if(iter.getValue().first == "no2")
        {
            EXPECT_EQ(3, iter.getValue().second);
        }
        else if(iter.getValue().first == "no3")
        {
            EXPECT_EQ(4, iter.getValue().second);
        }
        else if(iter.getValue().first == "no4")
        {
            EXPECT_EQ(5, iter.getValue().second);
        }
        else if(iter.getValue().first == "no5")
        {
            EXPECT_EQ(6, iter.getValue().second);
        }
        else if(iter.getValue().first == "no6")
        {
            EXPECT_EQ(64, iter.getValue().second);
        }
        else
        {
            EXPECT_EQ("", iter.getValue().first);
        }
        iter = iter.getNext();
    }
    EXPECT_EQ(8, count);
}
