/**
 * list.hpp
 * 
 * <P> Implementation of a singly linked list.
 * 
 * @author gchapman
 */

#ifndef LIST_HPP_145800
#define	LIST_HPP_145800

template<class value>
class list_node
{
public:
    
    /**
     * Will construct the node with a value.
     * @param v Value for the node to copy.
     */
    list_node(const value& v);
    
    /**
     * @return Reference value of the node
     */
    value& getValue();
    
    /**
     * @return The next node linked from this node.
     */
    list_node* getNext();
    
    /**
     * Will set the next node.
     * @param next The next node of this node.
     */
    void setNext(list_node* next);
    
    /**
     * @return True if this is the last node in the list.
     */
    bool isEnd();
    
private:
    
    list_node* mNext;
    value mValue;
};

template<class value>
list_node<value>::list_node(const value& v) : 
    mNext(NULL),
    mValue(v)
{
}

template<class value>
value& list_node<value>::getValue()
{
    return mValue;
}

template<class value>
list_node<value>* list_node<value>::getNext()
{
    return mNext;
}

template<class value>
void list_node<value>::setNext(list_node* next)
{
    mNext = next;
}

template<class value>
bool list_node<value>::isEnd()
{
    return mNext == NULL;
}

template<class value>
class list;

template<class value>
class list_iterator
{
    friend class list<value>;
    
public:
    
    /**
     * Constructs an empty iterator
     */
    list_iterator() :
        mPrevious(NULL),
        mCurrent(NULL),
        mListId(NULL)
    {
    }
    
    /**
     * Constructs an iterator that has list content.
     * @param prev Previously iterated node.
     * @param current Current node.
     * @param listHead List identification for iterators.
     */
    list_iterator(list_node<value>* prev, list_node<value>* current, 
                    list_node<value>* listHead) : 
        mPrevious(prev),
        mCurrent(current),
        mListId(listHead)
    {
    }
    
    /**
     * @return Will get the value of the current node or null.
     */
    value* getValue()
    {
        if(mCurrent != NULL)
        {
            return &mCurrent->getValue();
        }
        return NULL;
    }
    
    /**
     * @return True if this is an ending iterator.
     */
    bool isEnd()
    {
        return mCurrent == NULL;
    }
    
    /**
     * @return Will get the next iterator.
     */
    list_iterator getNext()
    {
        if(mCurrent != NULL)
        {
            return list_iterator(mCurrent, mCurrent->getNext(), mListId);
        }
        return list_iterator();
    }
    
private:
    list_node<value>* mPrevious;
    list_node<value>* mCurrent;
    list_node<value>* mListId;
};

template<class value>
class list
{
public:
    typedef list_iterator<value> iterator;
    
    /**
     * Constructs an empty list.
     */
    list();
    
    /**
     * The copy constructor.
     * @param copy Will copy this list.
     */
    list(const list& copy);
    
    /**
     * Will provide copy function.
     * @param copy Will copy this list and values.
     * @return 
     */
    list& operator=(const list& copy);
    
    /**
     * Destructor, destroys all nodes
     */
    virtual ~list();
    
    /**
     * Will destroy all nodes.
     */
    void eraseAll();
    
    /**
     * Will erase the iterator position in the list. 
     * This will invalidate other iterators.
     * Note: While this operation is O(1), if erase(back()) is performed
     * it will be O(n) operation.
     * @param eraseIter Iterator to use for erase.
     * @return The next iterator. 
     */
    iterator erase(const iterator& eraseIter);
    
    /**
     * Will push back the value on the end of the list, O(1)
     * @param v Value to push back.
     */
    void pushBack(const value& v);
    
    /**
     * Will insert the value at the front of the list. O(1)
     * Invalidates iterators.
     * @param v Value to insert.
     */
    void insert(const value& v);
    
    /**
     * Will insert the value in an ascending order using operator>
     * Invalidates iterators.
     * @param v Value to insert in ascending order. O(n)
     */
    void insertSorted(const value& v);
    
    /**
     * @return Iterator at the beginning of the list.
     */
    iterator begin() const;
    
    /**
     * @return Iterator of the back most entry. O(1)
     */
    iterator back();
    
protected:
    
    bool eraseBack(const iterator& eraseIter, iterator& returnIter);
    bool eraseHead(const iterator& eraseIter, iterator& returnIter);
    bool eraseTail(const iterator& eraseIter, iterator& returnIter);
    
    list_node<value>* mHead;
    list_node<value>* mTail;
};

template<class value>
list<value>::list() :
    mHead(NULL), 
    mTail(NULL)
{
}

template<class value>
list<value>::~list()
{
    eraseAll();
}

template<class value>
list<value>::list(const list<value>& copy) :
    mHead(NULL), 
    mTail(NULL)
{
    *this = copy;
}
    
template<class value>
list<value>& list<value>::operator=(const list<value>& copy)
{
    eraseAll();
    list<value>::iterator iter = copy.begin();
    while(!iter.isEnd())
    {
        pushBack(*iter.getValue());
        iter = iter.getNext();
    }
    return *this;
}

template<class value>
void list<value>::eraseAll()
{
    list_node<value>* deleteNode = mHead;
    while(deleteNode != NULL) 
    {
        list_node<value>* tmp = deleteNode->getNext();
        delete deleteNode;
        deleteNode = tmp;
    }
    mHead = NULL;
    mTail = NULL;
}

template<class value>
typename list<value>::iterator list<value>::erase(const iterator& eraseIter)
{
    typename list<value>::iterator returnIterator;
    if(eraseIter.mListId == mHead && mHead != NULL) //check this iterator is of this list
    {
        bool erasedEdgeCases = eraseBack(eraseIter, returnIterator) ||
                               eraseHead(eraseIter, returnIterator) ||
                               eraseTail(eraseIter, returnIterator);
        if(!erasedEdgeCases)
        {
            if(eraseIter.mPrevious != NULL && eraseIter.mCurrent != NULL)
            {
                eraseIter.mPrevious->setNext(eraseIter.mCurrent->getNext());
                returnIterator = iterator(eraseIter.mPrevious, eraseIter.mPrevious->getNext(), mHead);
                delete eraseIter.mCurrent;
            }
        }
    }
    return returnIterator;
}

template<class value>
bool list<value>::eraseTail(const iterator& eraseIter, iterator& returnIter)
{
    if(eraseIter.mCurrent == mTail)
    {
        if(mTail == mHead) //if tail is the head, set head to null now
        {
            mHead = NULL; 
        }
        if(eraseIter.mPrevious != NULL)
        {
            eraseIter.mPrevious->setNext(NULL);
            mTail = eraseIter.mPrevious;
        }
        returnIter = iterator(eraseIter.mPrevious, NULL, mHead);
        delete eraseIter.mCurrent;
        return true;
    }
    return false;
}

template<class value>
bool list<value>::eraseHead(const iterator& eraseIter, iterator& returnIter)
{
    if(eraseIter.mCurrent == mHead)
    {
        if(mTail == mHead) //if tail is the head, set tail to null now
        {
            mTail = NULL; 
        }
        mHead = mHead->getNext();
        returnIter = iterator(NULL, mHead, mHead);
        delete eraseIter.mCurrent;
        return true;
    }
    return false;
}

template<class value>
bool list<value>::eraseBack(const iterator& eraseIter, iterator& returnIter)
{
    //case where the back() iterator was given to erase, no way to get back to
    //the previous to erase correctly, need to iterate O(n)
    if(eraseIter.mPrevious == NULL && eraseIter.mCurrent == mTail 
       && eraseIter.mCurrent != mHead)
    {
        iterator iter = begin();
        while(!iter.isEnd())
        {
            if(eraseIter.mCurrent == iter.mCurrent)
            {
                returnIter = erase(iter); //recurse call, now should end up in
                                          //eraseHead or eraseTail
                break;
            }
            iter = iter.getNext();
        }
        return true;
    }
    return false;
}

template<class value>
typename list<value>::iterator list<value>::back() 
{
    return list<value>::iterator(NULL, mTail, mHead);
}

template<class value>
void list<value>::pushBack(const value& v)
{
    list_node<value>* insert = new list_node<value>(v);
    if(mHead == NULL)
    {
        mHead = insert;
        mTail = insert;
    }
    else
    {
        mTail->setNext(insert);
        mTail = insert;
    }
}

template<class value>
void list<value>::insert(const value& v)
{
    list_node<value>* insert = new list_node<value>(v);
    if(mHead == NULL)
    {
        mHead = insert;
        mTail = insert;
    }
    else
    {
        list_node<value>* next = mHead;
        mHead = insert;
        insert->setNext(next);
    }
}

template<class value>
typename list<value>::iterator list<value>::begin() const
{
    return list<value>::iterator(NULL, mHead, mHead);
}

template<class value>
void list<value>::insertSorted(const value& v)
{
    bool inserted = false;
    list<value>::iterator iter = begin();
    while(!iter.isEnd() && !inserted)
    {
        if(*iter.getValue() > v)
        {
            inserted = true;
            if(iter.mPrevious != NULL)
            {
                list_node<value>* insertNode = new list_node<value>(v);
                iter.mPrevious->setNext(insertNode);
                insertNode->setNext(iter.mCurrent);
            }
            else //working with head
            {
                insert(v);
            }
        }
        iter = iter.getNext();
    }
    //if we are at the end but didn't insert, pushBack
    if(!inserted)
    {
        pushBack(v);
    }
}

#endif	/* LIST_HPP_145800 */

