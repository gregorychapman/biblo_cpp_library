/**
 * pair.hpp
 * 
 * <P> Implementation of a simple pair.
 * 
 * @author gchapman
 */

#ifndef PAIR_HPP_145800
#define	PAIR_HPP_145800

template<class value1, class value2>
class pair
{
public:
    /**
     * Construct the pair with two values
     * @param v1 Value 1
     * @param v2 Value 2
     */
    pair(const value1& v1, const value2& v2)
    {
        first = v1;
        second = v2;
    }
    
    value1 first;
    value2 second;
};

#endif	/* PAIR_HPP_145800 */

