# README #

Personal library for C++ code. Before you say it, yes, I use C++11 and boost. This is my playground to make algorithms, data structures, and it also comes in handy for those times when you have restrictions on using external libraries, i.e. when you are in an code interview and you need to develop the code "from scratch."
